import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import AjouterPharmacie from './components/AjouterPharmacie';
import ListePharmacieGarde from './components/ListePharmacieGarde';
import Pharmacie from './components/Pharmacie';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <h2>Enfin cela fonctionne je suis au ange mdr !</h2>
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <ul className="navbar-nav mr-auto">
              <li><Link to={'/AjouterPharmacie'} className="nav-link">AjoutPharmacie</Link></li>
              <li><Link to={'/LIstePharmacieGarde'} className="nav-link">ListePharmacieGarde</Link></li>
              <li><Link to={'/Pharmacie'} className="nav-link">Pharmacie</Link></li>
            </ul>
          </nav>
          <hr />
          <Switch>
            <Route exact path='/AjouterPharmacie' component={AjouterPharmacie} />
            <Route path='/ListePharmacieGarde' component={ListePharmacieGarde} />
            <Route path='/Pharmacie' component={Pharmacie} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
