import React, { Component } from 'react';
import axios from 'axios';

class AjouterPharmacie extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nom: '',
      ville: '',
      quartier: '',
      garde: ''
    };

    this.handleChangeInput = this.handleChangeInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeInput(event) {
    console.log(event.target.value)
    console.log(event.target.name)
    this.setState({
      [event.target.name]: event.target.value
    });
  }
  handleSubmit(event) {
    event.preventDefault();
    axios.post('http://127.0.0.1:8000/pharma', this.state).then(
      () => {
        alert('La c\'est vraiment enregistré')
      }
    )
      .catch(e => {
        alert('ca marche po')
      })

  }
  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <div>
            <p><label>Renseigner le nom de la pharmacie:</label></p>
            <p><input type="text" name="nom" placeholder="Nom" value={this.state.nom} onChange={this.handleChangeInput} required /></p>
            <p><label>Quartier de la pharmacie:</label></p>
            <p><input type="text" name="quartier" placeholder="Le quartier de la pharmacie" value={this.state.Quartier} onChange={this.handleChangeInput} required /></p>
            <p><label>La ville de la pharmacie:</label></p>
            <p><input type="text" name="ville" placeholder="Ville de la pharmacie" value={this.state.Ville} onChange={this.handleChangeInput} required /></p>
          </div>
          <p><label>Les jours de gardes:</label></p>
          <select name='garde' value={this.state.garde} onChange={this.handleChangeInput}>
            <option value="*" required>Choisir</option>
            <option value="jours">Lundi</option>
            <option value="jours">Mardi</option>
            <option value="jours">Mercredi</option>
            <option value="jours">Jeudi</option>
            <option value="jours">Vendredi</option>
          </select>
          <br />
          <p><button type="submit" value="Envoyer">Soumettre</button></p>
        </form>
      </div>
    );
  }
}

export default AjouterPharmacie;