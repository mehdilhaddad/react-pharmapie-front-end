function Pharma(props) {

    return (
        <li key={props.fiche.id}>
            <div>
                <strong>{props.fiche.nom}</strong>
                <br />
                {props.fiche.quartier} /&nbsp;
                {props.fiche.ville}
            </div>
            <div>{props.fiche.garde}</div>
        </li >
    );
}

export default Pharma;