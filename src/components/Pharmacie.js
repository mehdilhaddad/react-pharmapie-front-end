import React, { Component } from 'react';
import axios from 'axios';
import Pharma from './Pharma';
class PharmaList extends Component {

  constructor(props) {
      super(props);
      this.state = { pharmacies: [] };
  }

  componentDidMount() {
      axios.get('http://127.0.0.1:8000/pharma')
          .then(pharmapi => {
              console.log(pharmapi.data);
              this.setState({ pharmacies: pharmapi.data });
          });
  }
  render() {
      return (
          <ul>
              {this.state.pharmacies
                  .map(pharmacie => (
                      <Pharma key={pharmacie.id} fiche={pharmacie} />
                  ))}
          </ul>
      )
  }
}

export default PharmaList;